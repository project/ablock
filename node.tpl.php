<?php // $Id$ ?>

<div class="node" id="node-<?php print $node->nid; ?>">
  			
		<?php if ($page == 0): ?>
    	<h2 class="title">
      	<a href="<?php print $node_url ?>"><?php print $title; ?></a>
    	</h2>
  	<?php endif; ?>

	<div class="node-info">
  
  	<?php if ($submitted): ?>
    	<span class="submitted"><?php print t('Posted on ') . format_date($node->created, 'custom', "F jS, Y") . t(' by ') . theme('username', $node); ?></span> 
  	<?php endif; ?>

  	<?php if (count($taxonomy)): ?>
    	<div class="taxonomy"><?php print t(' in ') . $terms ?></div>
  	<?php endif; ?>

	  <?php if ($links): ?>
	    <div class="links">
	      <?php print $links; ?>
	    </div>
	  <?php endif; ?>


  </div>


  <div class="content">
	  <?php if ($picture): ?>
			<?php print $picture; ?>  
  	<?php endif; ?>
    <?php print $content; ?>
    <div class="clear-block"></div>
  </div>
   
</div>
