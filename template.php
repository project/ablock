<?php
 
function ablock_regions() {
  return array(
       'content_top' => t('content top'),
       'content_bottom' => t('content bottom'),
       'footer' => t('footer')
  );
} 

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function ablock_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div id="breadcrumb">'. implode(' ', $breadcrumb) .'</div>';
  }
}