<?php // $Id$ ?>

<div class="comment<?php if ($comment->status == COMMENT_NOT_PUBLISHED) print ' comment-unpublished'; ?>">
	
<?php if ($new != '') { ?>
	<span class="new"><?php print $new; ?></span>
<?php } ?>

<h3 class="title"><?php print $title; ?></h3>

<div class="node-info">

	<?php if ($submitted): ?>
    <div class="submitted"><?php print t('Posted on ') . format_date($comment->timestamp, 'custom', "F jS, Y") . t(' by ') . theme('username', $comment); ?></div>
	<?php endif; ?>

  <?php if ($links): ?>
    <div class="links">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

</div>

<div class="content">
  <?php if ($picture): ?>
		<?php print $picture; ?>  
	<?php endif; ?>
	<?php print $content; ?>
  <div class="clear-block"></div>
</div>

</div>